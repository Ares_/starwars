<?php

namespace App\Form;

use App\Entity\Person;
use App\Entity\Planete;
use App\Entity\Species;
use App\Entity\Starship;
use App\Entity\Vehicule;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('height')
            ->add('masse')
            ->add('gender')
            ->add('planete', EntityType::class, [
                'class' => Planete::class,
                'choice_label' => 'name'
                
            ])
            ->add('vehicule', EntityType::class, [
                'class' => Vehicule::class,
                'choice_label' => 'name'
            ])
            ->add('starships', EntityType::class, [
                'class' => Starship::class,
                'choice_label' => 'name'
            ])
            ->add('specy_id', EntityType::class, [
                'class' => Species::class,
                'choice_label' => 'name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Person::class,
        ]);
    }
}
