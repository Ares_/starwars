<?php


namespace App\Manager;

use App\Entity\Vehicule;
use App\Repository\SpeciesRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Person;
use App\Entity\Planete;
use App\Entity\Species;
use App\Entity\Starship;
use App\Repository\PlaneteRepository;
use App\Repository\StarshipRepository;
use App\Repository\VehiculeRepository;
use Symfony\Component\HttpClient\HttpClient;

class Manager
{
    public function __construct(EntityManagerInterface $entityManager,
                                PlaneteRepository $planete,
                                SpeciesRepository $specy,
                                VehiculeRepository $vehicule,
                                StarshipRepository $starship)
    {
        $this->starship = $starship;
        $this->vehicule = $vehicule;
        $this->planete = $planete;
        $this->specy = $specy;
        $this->entity = $entityManager;
    }

    private function handlePerson($json)
    {
        foreach ($json->results as $key => $value) {
            try{
                $person = new Person();
                if($value->homeworld != null){
                    $planeteRef = preg_replace('/\D/', '', $value->homeworld);
                }
                if($value->species != null){
                    $specyRef = preg_replace('/\D/', '', $value->species);
                }
                if ($planeteRef) {
                    $planete = $this->planete->findOneByRef($planeteRef);
                    if ($planete) {
                        $person->setPlanete($planete);
                    }
                }
                if ($specyRef) {
                    $specy = $this->specy->findOneByRef($specyRef);
                    if ($specy) {
                        $person->setSpecyId($specy);
                    }
                }

                if (!is_numeric($value->height)) {
                    $value->height = null;
                }
                if (!is_numeric($value->mass)) {
                    $value->mass = null;
                }
                $person->setName($value->name);
                $person->setGender($value->gender);
                $person->setHeight($value->height);
                $person->setMasse($value->mass);
                if($value->vehicles != null){
                    foreach ($value->vehicles as $ref) {
                        $vehicule = preg_replace('/\D/', '', $ref);
                        if ($vehicule) {
                            $vehicule = $this->vehicule->findOneByRef($vehicule);
                            if ($vehicule) {
                                $person->addVehicule($vehicule);
                            }
                        }
                    }
                }
                if($value->starships != null){
                    foreach ($value->starships as $ref) {
                        $starshipRef = preg_replace('/\D/', '', $ref);
                        if ($starshipRef) {
                            $starship = $this->starship->findOneByRef($starshipRef);
                            if ($starship) {
                                $person->addStarship($starship);
                            }
                        }
                    }
                }
                
                $this->entity->persist($person);
            }catch(\Throwable $th) {
                dd($value, $th->getMessage(), $th->getLine());
            }
        }
    }

    private function handlePlanets($json)
    {
        foreach ($json->results as $key => $value) {
            $planete = new Planete();
            $planete->setName($value->name);
            $ref = preg_replace('/\D/', '', $value->url);
            $planete->setRef((int)$ref);
            $planete->setClimate($value->climate);
            if (!is_numeric($value->population)) {
                $value->population = null;
            }
            if (!is_numeric($value->diameter)) {
                $value->diameter = null;
            }
            $planete->setDiameter($value->diameter);
            if (!is_numeric($value->population)) {
                $value->population = null;
            }
            $planete->setPopulation($value->population);
            $this->entity->persist($planete);
        }
    }

    private function handleSpecies($json)
    {
        foreach ($json->results as $key => $value) {
            $species = new Species();
            $species->setName($value->name);
            $ref = preg_replace('/\D/', '', $value->url);
            $species->setRef((int)$ref);
            $species->setClassification($value->classification);
            $this->entity->persist($species);
        }
    }

    private function handleStarships($json)
    {
        foreach ($json->results as $key => $value) {
            $Starships = new Starship();
            $Starships->setName($value->name);
            $ref = preg_replace('/\D/', '', $value->url);
            $Starships->setRef($ref);
            if (!is_numeric($value->hyperdrive_rating)) {
                $value->hyperdrive_rating = null;
            }
            $Starships->setHyperdriveRating($value->hyperdrive_rating);
            if (!is_numeric($value->length)) {
                $value->length = null;
            }
            $Starships->setLength($value->length);
            $this->entity->persist($Starships);
        }
    }

    private function handleVehicles($json)
    {
        foreach ($json->results as $key => $value) {
            $Vehicule = new Vehicule();
            $Vehicule->setName($value->name);
            if (!is_numeric($value->length)) {
                $value->length = null;
            }
            $ref = preg_replace('/\D/', '', $value->url);
            $Vehicule->setRef($ref);
            $Vehicule->setLength($value->length);
            $this->entity->persist($Vehicule);
        }
    }

    public function decode($json, $handler)
    {
        $json = json_decode($json, false);
        while ($json->next != null) {
            if ($json->count > 0) {
                $method_name_as_string = 'handle' . ucfirst($handler);
                $this->$method_name_as_string($json);
            }
            $httpClient = HttpClient::create();
            $response = $httpClient->request('GET', $json->next);
            $json = json_decode($response->getContent(), false);
        }
        $this->entity->flush();
    }
}