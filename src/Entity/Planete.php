<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="planete")
 * @ORM\Entity(repositoryClass="App\Repository\PlaneteRepository")
 */
class Planete
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(name="diameter", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $diameter;

    /**
     * @ORM\Column(name="climate", type="string", length=255, nullable=true)
     */
    private $climate;

    /**
     * @ORM\Column(name="population", type="bigint", nullable=true)
     */
    private $population;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $edit_at;

    /**
     * @ORM\Column(name="ref", type="integer")
     */
    private $ref;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDiameter(): ?string
    {
        return $this->diameter;
    }

    public function setDiameter(?string $diameter): self
    {
        $this->diameter = $diameter;

        return $this;
    }

    public function getClimate(): ?string
    {
        return $this->climate;
    }

    public function setClimate(?string $climate): self
    {
        $this->climate = $climate;

        return $this;
    }

    public function getPopulation(): ?int
    {
        return $this->population;
    }

    public function setPopulation(?int $population): self
    {
        $this->population = $population;

        return $this;
    }


    public function getEditAt(): ?\DateTimeInterface
    {
        return $this->edit_at;
    }

    public function setEditAt(?\DateTimeInterface $edit_at): self
    {
        $this->edit_at = $edit_at;

        return $this;
    }

    public function getRef(): ?int
    {
        return $this->ref;
    }

    public function setRef(int $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

}
