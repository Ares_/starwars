<?php

namespace App\Entity;

use App\Entity\Planete;
use App\Entity\Species;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="person")
 * @ORM\Entity(repositoryClass="App\Repository\PersonRepository")
 */
class Person
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(name="height", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $height;

    /**
     * @ORM\Column(name="masse", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $masse;

    /**
     * @ORM\Column(name="gender", type="string", length=30)
     */
    private $gender;

    /**
     * @var Planete
     *
     * @ORM\ManyToOne(targetEntity="Planete")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="planete_id", referencedColumnName="id")
     * })
     */
    private $planete;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Vehicule", inversedBy="person")
     * @ORM\JoinTable(name="person_vehicule",
     *   joinColumns={
     *     @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="vehicule_id", referencedColumnName="id")
     *   }
     * )
     */
    private $vehicule;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Starship", inversedBy="person")
     * @ORM\JoinTable(name="person_starship",
     *   joinColumns={
     *     @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="starship_id", referencedColumnName="id")
     *   }
     * )
     */
    private $starships;

    /**
     * @var Species
     *
     * @ORM\ManyToOne(targetEntity="Species")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="specy_id", referencedColumnName="id")
     * })
     */
    private $specy_id;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $edit_at;

    public function __construct()
    {
        $this->vehicule = new ArrayCollection();
        $this->starships = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->height;
    }

    public function setHeight(?string $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getMasse(): ?string
    {
        return $this->masse;
    }

    public function setMasse(?string $masse): self
    {
        $this->masse = $masse;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getPlanete()
    {
        return $this->planete;
    }

    public function setPlanete(?Planete $planete): self
    {
        $this->planete = $planete;

        return $this;
    }

    /**
     * @return Collection|Vehicule[]
     */
    public function getVehicule(): Collection
    {
        return $this->vehicule;
    }

    public function addVehicule(Vehicule $vehicule): self
    {
        if (!$this->vehicule->contains($vehicule)) {
            $this->vehicule[] = $vehicule;
        }

        return $this;
    }

    public function removeVehicule(Vehicule $vehicule): self
    {
        if ($this->vehicule->contains($vehicule)) {
            $this->vehicule->removeElement($vehicule);
        }

        return $this;
    }

    /**
     * @return Collection|Starship[]
     */
    public function getStarships(): Collection
    {
        return $this->starships;
    }

    public function addStarship(StarShip $starship): self
    {
        if (!$this->starships->contains($starship)) {
            $this->starships[] = $starship;
        }

        return $this;
    }

    public function removeStarship(StarShip $starship): self
    {
        if ($this->starships->contains($starship)) {
            $this->starships->removeElement($starship);
        }

        return $this;
    }

    public function getSpecyId()
    {
        return $this->specy_id;
    }

    public function setSpecyId(?species $specy_id): self
    {
        $this->specy_id = $specy_id;

        return $this;
    }


    public function getEditAt(): ?\DateTimeInterface
    {
        return $this->edit_at;
    }

    public function setEditAt(?\DateTimeInterface $edit_at): self
    {
        $this->edit_at = $edit_at;

        return $this;
    }

}
