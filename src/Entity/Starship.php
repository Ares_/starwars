<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Starship
 *
 * @ORM\Table(name="starship")
 * @ORM\Entity(repositoryClass="App\Repository\StarshipRepository")
 */
class Starship
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(name="length", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $length;

    /**
     * @ORM\Column(name="hyperdrive_rating", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $hyperdrive_rating;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Person", mappedBy="starship")
     */
    private $person;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $edit_at;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ref;

    public function __construct()
    {
        $this->person = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLength(): ?string
    {
        return $this->length;
    }

    public function setLength(?string $length): self
    {
        $this->length = $length;

        return $this;
    }

    public function getHyperdriveRating(): ?string
    {
        return $this->hyperdrive_rating;
    }

    public function setHyperdriveRating(?string $hyperdrive_rating): self
    {
        $this->hyperdrive_rating = $hyperdrive_rating;

        return $this;
    }

    /**
     * @return Collection|Person[]
     */
    public function getPerson(): Collection
    {
        return $this->person;
    }

    public function addPerson(Person $person): self
    {
        if (!$this->person->contains($person)) {
            $this->person[] = $person;
            $person->addStarship($this);
        }

        return $this;
    }

    public function removePerson(Person $person): self
    {
        if ($this->person->contains($person)) {
            $this->person->removeElement($person);
            $person->removeStarship($this);
        }

        return $this;
    }


    public function getEditAt(): ?\DateTimeInterface
    {
        return $this->edit_at;
    }

    public function setEditAt(?\DateTimeInterface $edit_at): self
    {
        $this->edit_at = $edit_at;

        return $this;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function setRef(string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }
}
