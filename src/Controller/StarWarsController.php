<?php

namespace App\Controller;

use App\Entity\Person;
use App\Entity\Planete;
use App\Entity\Species;
use App\Entity\Starship;
use App\Entity\Vehicule;
use App\Form\PlaneteType;
use App\Form\SpeciesType;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class StarWarsController extends AbstractController
{
    /**
     * @Route("/star-wars", name="star_wars")
     */
    public function index()
    {
        // $t = $this->getDoctrine()->getRepository(Person::class)->find(561);
        
        // $v = $t->getVehicule()->getValues()[0]->getName();
        // dd($v);
        $d = $this->getDoctrine()->getRepository(Person::class)->plusUnPoint();
        dd($d);

        return $this->render('star_wars/index.html.twig', [
            'controller_name' => 'StarWarsController',
        ]);
    }

    /**
     * @Route("/search", name="search_person", methods={"GET","POST"})
     */
    public function new(Request $request, $planete = null, $species = null, $person = null): Response
    {
        
        $defaultData = ['message' => 'Type your message here'];
        $form = $this->createFormBuilder($defaultData)
        ->add('planete', EntityType::class, [
            'class' => Planete::class,
            'choice_label' => 'name'
        ])
        ->add('species', EntityType::class
        , [
            'class' => Species::class,
            'choice_label' => 'name'
        ])
        ->add('Search', TextType::class)
        ->getForm();

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $result = $this->getDoctrine()->getRepository(Person::class)->searchView($planete, $species, $person);

            return $this->render('star_wars/search.html.twig', [
                'form' => $form->createView(),
                'result' => $result
            ]);
        }

        return $this->render('star_wars/search.html.twig', [
            'form' => $form->createView(),
            'result' => null
        ]);
    }

}
