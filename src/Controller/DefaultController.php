<?php

namespace App\Controller;

use App\Entity\Person;
use App\Manager\Manager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpClient\HttpClient;

/**
 * @Route("/init")
 */
class DefaultController extends AbstractController
{
    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/person", name="person")
     */
    public function getPeople()
    {
        $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', 'https://swapi.co/api/people/');
        $content = $response->getContent();
        $content = $this->manager->decode($content, 'Person');

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController'
        ]);

    }

    /**
     * @Route("/planete", name="planete")
     */
    public function getPlanets()
    {
        $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', 'https://swapi.co/api/planets/');
        $content = $response->getContent();
        $content = $this->manager->decode($content, 'Planets');

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController'
        ]);
    }

    /**
     * @Route("/species", name="species")
     */
    public function getSpecies()
    {
        $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', 'https://swapi.co/api/species/');
        $content = $response->getContent();
        $content = $this->manager->decode($content, 'Species');

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController'
        ]);

    }

    /**
     * @Route("/starship", name="starship")
     */
    public function getStarships()
    {
        $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', 'https://swapi.co/api/starships/');
        $content = $response->getContent();
        $content = $this->manager->decode($content, 'Starships');

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController'
        ]);

    }

    /**
     * @Route("/vehicule", name="vehicule")
     */
    public function getVehicules()
    {
        $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', 'https://swapi.co/api/vehicles/');
        $content = $response->getContent();
        $content = $this->manager->decode($content, 'Vehicles');

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController'
        ]);

    }
    /**
     * @Route("/default", name="default")
     */
    public function index()
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'content' => $this->getSpecies()
        ]);

    }
}
