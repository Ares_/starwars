<?php

namespace App\Controller;

use App\Entity\Starship;
use App\Form\StarshipType;
use App\Repository\StarshipRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/starship")
 */
class StarshipController extends AbstractController
{
    /**
     * @Route("/", name="starship_index", methods={"GET"})
     */
    public function index(StarshipRepository $starshipRepository): Response
    {
        return $this->render('starship/index.html.twig', [
            'starships' => $starshipRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="starship_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $starship = new Starship();
        $form = $this->createForm(StarshipType::class, $starship);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($starship);
            $entityManager->flush();

            return $this->redirectToRoute('starship_index');
        }

        return $this->render('starship/new.html.twig', [
            'starship' => $starship,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="starship_show", methods={"GET"})
     */
    public function show(Starship $starship): Response
    {
        return $this->render('starship/show.html.twig', [
            'starship' => $starship,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="starship_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Starship $starship): Response
    {
        $form = $this->createForm(StarshipType::class, $starship);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('starship_index');
        }

        return $this->render('starship/edit.html.twig', [
            'starship' => $starship,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="starship_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Starship $starship): Response
    {
        if ($this->isCsrfTokenValid('delete'.$starship->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($starship);
            $entityManager->flush();
        }

        return $this->redirectToRoute('starship_index');
    }
}
