<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200130132014 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE planete (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, diameter NUMERIC(10, 2) DEFAULT NULL, climate VARCHAR(255) DEFAULT NULL, population INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE species (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(200) NOT NULL, classification VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person (id INT AUTO_INCREMENT NOT NULL, planete_id INT DEFAULT NULL, specy_id INT DEFAULT NULL, name VARCHAR(50) NOT NULL, height NUMERIC(10, 2) DEFAULT NULL, masse NUMERIC(10, 2) DEFAULT NULL, gender VARCHAR(30) NOT NULL, INDEX IDX_34DCD176A9CFCB36 (planete_id), INDEX IDX_34DCD176A209F971 (specy_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person_vehicule (person_id INT NOT NULL, vehicule_id INT NOT NULL, INDEX IDX_BD695A2A217BBB47 (person_id), INDEX IDX_BD695A2A4A4A3511 (vehicule_id), PRIMARY KEY(person_id, vehicule_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person_starship (person_id INT NOT NULL, starship_id INT NOT NULL, INDEX IDX_5052437D217BBB47 (person_id), INDEX IDX_5052437D9B24DF5 (starship_id), PRIMARY KEY(person_id, starship_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE starship (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, length NUMERIC(10, 2) DEFAULT NULL, hyperdrive_rating NUMERIC(10, 2) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vehicule (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, length NUMERIC(10, 2) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD176A9CFCB36 FOREIGN KEY (planete_id) REFERENCES planete (id)');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD176A209F971 FOREIGN KEY (specy_id) REFERENCES species (id)');
        $this->addSql('ALTER TABLE person_vehicule ADD CONSTRAINT FK_BD695A2A217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE person_vehicule ADD CONSTRAINT FK_BD695A2A4A4A3511 FOREIGN KEY (vehicule_id) REFERENCES vehicule (id)');
        $this->addSql('ALTER TABLE person_starship ADD CONSTRAINT FK_5052437D217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE person_starship ADD CONSTRAINT FK_5052437D9B24DF5 FOREIGN KEY (starship_id) REFERENCES starship (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE person DROP FOREIGN KEY FK_34DCD176A9CFCB36');
        $this->addSql('ALTER TABLE person DROP FOREIGN KEY FK_34DCD176A209F971');
        $this->addSql('ALTER TABLE person_vehicule DROP FOREIGN KEY FK_BD695A2A217BBB47');
        $this->addSql('ALTER TABLE person_starship DROP FOREIGN KEY FK_5052437D217BBB47');
        $this->addSql('ALTER TABLE person_starship DROP FOREIGN KEY FK_5052437D9B24DF5');
        $this->addSql('ALTER TABLE person_vehicule DROP FOREIGN KEY FK_BD695A2A4A4A3511');
        $this->addSql('DROP TABLE planete');
        $this->addSql('DROP TABLE species');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP TABLE person_vehicule');
        $this->addSql('DROP TABLE person_starship');
        $this->addSql('DROP TABLE starship');
        $this->addSql('DROP TABLE vehicule');
    }
}
