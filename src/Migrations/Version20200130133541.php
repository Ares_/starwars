<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200130133541 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE person CHANGE planete_id planete_id INT DEFAULT NULL, CHANGE specy_id specy_id INT DEFAULT NULL, CHANGE height height NUMERIC(10, 2) DEFAULT NULL, CHANGE masse masse NUMERIC(10, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE planete CHANGE diameter diameter NUMERIC(10, 2) DEFAULT NULL, CHANGE climate climate VARCHAR(255) DEFAULT NULL, CHANGE population population BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE species CHANGE classification classification VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE starship CHANGE length length NUMERIC(10, 2) DEFAULT NULL, CHANGE hyperdrive_rating hyperdrive_rating NUMERIC(10, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE vehicule CHANGE length length NUMERIC(10, 2) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE person CHANGE planete_id planete_id INT DEFAULT NULL, CHANGE specy_id specy_id INT DEFAULT NULL, CHANGE height height NUMERIC(10, 2) DEFAULT \'NULL\', CHANGE masse masse NUMERIC(10, 2) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE planete CHANGE diameter diameter NUMERIC(10, 2) DEFAULT \'NULL\', CHANGE climate climate VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE population population INT DEFAULT NULL');
        $this->addSql('ALTER TABLE species CHANGE classification classification VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE starship CHANGE length length NUMERIC(10, 2) DEFAULT \'NULL\', CHANGE hyperdrive_rating hyperdrive_rating NUMERIC(10, 2) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE vehicule CHANGE length length NUMERIC(10, 2) DEFAULT \'NULL\'');
    }
}
