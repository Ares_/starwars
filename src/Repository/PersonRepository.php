<?php

namespace App\Repository;

use App\Entity\Person;
use App\Entity\Species;
use App\Entity\Starship;
use App\Entity\Vehicule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Person|null find($id, $lockMode = null, $lockVersion = null)
 * @method Person|null findOneBy(array $criteria, array $orderBy = null)
 * @method Person[]    findAll()
 * @method Person[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Person::class);
    }

    public function plusUnPoint()
    {
        $specy = ['Droid', 'Human'];
        return $this->createQueryBuilder('p')
                    ->where('p.specy_id IN (:specy)')
                    
                    ->andWhere('p.vehicule = :vehicul')
                    ->orWhere('p.starship = :starship')
                    ->setParameters([
                        ':specy' => $specy,
                        ':vehicul' => 'Snowspeeder',
                        ':starship' => 'Slave 1'
                    ])
                    ->getQuery()
                    ->getResult();
    }

    public function searchView($planete, $species, $name)
    {
        return $this->createQueryBuilder('p')
                    ->where('p.name = :name')
                    ->andWhere('p.planete = :planete')
                    ->andWhere('p.specy_id = :species')
                    ->setParameters([
                        ':name' => $name,
                        ':planete' => $planete,
                        ':species' => $species
                    ])
                    ->getQuery()
                    ->getResult();
    }
    // /**
    //  * @return Person[] Returns an array of Person objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Person
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
